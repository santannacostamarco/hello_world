class ApplicationMailer < ActionMailer::Base
  default from: 'enviado@por.rails.app'
  layout 'mailer'
end
