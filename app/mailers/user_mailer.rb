class UserMailer < ApplicationMailer
    default from: 'enviado@por.rails.app'

    def testeEmail()
        mail(to: 'santannacostamarco@gmail.com', subject: 'Email teste Rails APP')
    end
end
