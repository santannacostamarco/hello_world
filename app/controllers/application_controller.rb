class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  def hello
    render html: "Olá, mundo!"
    UserMailer.testeEmail().deliver_later
  end
  def goodbye
    render html: "Até mais, mundo!"
  end
end
